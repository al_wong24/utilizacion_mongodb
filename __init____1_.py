import pymongo
import random

# Establece conexion con servidor MongoDB
con = pymongo.MongoClient("mongodb://localhost:27017/")

# Valida si la base de datos existe
dblist = con.list_database_names()
if "crm" in dblist:
    print("La base de datos existe")
else:
    print("No existe la base de datos")

db = con["crm"] # Base de datos
col = db["clientes"] # Coleccion

# Inserciones de un solo registro
cliente = { "nombre": "Juan", "direccion": "Panama Viejo"}
resultado = col.insert_one(cliente)
#print(resultado.inserted_id)

# Insercion de multiples registros
clientes = [ {"nombre": "Petra", "direccion": "Gonzalillo"}, {"nombre": "Susana", "direccion": "Capira", "edad": 50}, {"nombre": "Cristina"}]
resultado = col.insert_many(clientes)
#print(resultado.inserted_ids)

# Insercion de data con ID especifico
n = input("Nombre: ")
d = input("Direccion: ")
cliente = { "_id": random.randint(0,9999), "nombre": n, "direccion": d}
resultado = col.insert_one(cliente)
#print(resultado.inserted_id)

# Busqueda del primer registro
busqueda = col.find_one()
#print(busqueda)

# Busqueda de todos los registros
for cliente in col.find():
#    print(cliente)
    pass

# Busqueda incluyendo solamente el nombre
for cliente in col.find({},{ "_id": 0, "nombre": 1}):
#    print(cliente)
    pass

# Busqueda excluyendo la edad
for cliente in col.find({},{ "_id": 0, "edad": 0 }):
#    print(cliente)
    pass

# Busqueda con filtro
#idn = int(input("ID: "))
#query = { "_id": idn }
#resultado = col.find(query)
#for r in resultado:
#    print("Encontrado")
#    print(r)

# Busqueda con filtro de operador relacional
query = { "edad": { "$gt": 40 } }
resultado = col.find(query)
for r in resultado:
#    print(r)
    pass

# Busqueda con filtro de expresion regular
#print("Buscando REGEX")
query = { "direccion": { "$regex": "^V" } }
resultado = col.find(query)
for r in resultado:
#    print(r)
    pass

# Busqueda con multiples condiciones
query = { "$and": [ { "nombre": { "$ne": "Juan" }}, { "direccion": { "$regex": "^C"} } ]}
resultado = col.find(query)
for r in resultado:
#    print(r)
    pass

# Filtrado
resultado = col.find().sort("nombre", -1)
for r in resultado:
#    print(r)
    pass

# Borrar un registro
query = { "direccion": "Villa Lorena" }
col.delete_one(query)

# Borrar todos los registros
resultado = col.delete_many({})
print(str(resultado.deleted_count) +  " documentos eliminados.")

# Eliminar la coleccion
col.drop()
